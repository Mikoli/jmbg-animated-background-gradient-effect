"use strict";

/////////////////////////////////////////////////////////////////

//Funkcija koja vraća datum rođenja i potrebnu dužinu niza od 13 cifara

function datumRodjenja(jmbg) {
    let danRodjenja = jmbg.substring(0, 2); //index broja u nizu
    let mesecRodjenja = jmbg.substring(2, 4);
    let godinaRodjenja = "";
    let datum = danRodjenja + ". " + mesecRodjenja + ". " + godinaRodjenja + ".";

    if (jmbg[4] === "9" && jmbg.length == 13) {
        godinaRodjenja = "1" + jmbg.substring(4, 7);
        let datum = danRodjenja + ". " + mesecRodjenja + ". " + godinaRodjenja + ".";
        return document.getElementById("rezultat").innerHTML = "Vaš datum rođenja je " + datum;
    }
    else if (jmbg[4] === "0") {
        godinaRodjenja = "2" + jmbg.substring(4, 7);
        let datum = danRodjenja + ". " + mesecRodjenja + ". " + godinaRodjenja + ".";
        return document.getElementById("rezultat").innerHTML = "Vaš datum rođenja je " + datum;
    }
    else {
        return document.getElementById("rezultat").innerHTML = "Neispravan unos (unesite 13 cifara JMBG-a)";
    }
}

/////////////////////////////////////////////////////////////////

// Izbacuje današnji datum

// new Date().toLocaleDateString('en-us', { weekday:"long", year:"numeric", month:"short", day:"numeric"})
// "Friday, Jul 2, 2021"

////////////////////////////////////////

//Refreshing Input placeholder="Unesite Vaš JMBG"

function refresh() {
    document.getElementById('jmbg').value = "Unesite Vaš JMBG";
    document.getElementById('rezultat').innerHTML = "(Unesite 13 cifara JMBG-a)";
}

/////////////////////////////////////////////////////////////////

//Refreshing cele web strane, kao i Input placeholder="Unesite Vaš JMBG"

/* function refresh() {
    document.querySelector(".jmbg").value = window.location.reload();
} */

//OR

/* function refresh() {    
    setTimeout(function () {
        location.reload()
    }, 100);
} */